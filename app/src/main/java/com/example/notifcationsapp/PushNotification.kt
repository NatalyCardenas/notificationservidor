package com.example.notifcationsapp

data class PushNotification (
    val data: NotificationData,
    val to:String
)
