package com.example.notifcationsapp

data class NotificationData (
    val title: String,
    val message: String

)
